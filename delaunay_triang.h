#pragma once

#include <vector>
#include <cstddef>
#include <cstdbool>

#include "Linear_Algebra/vector.h"

struct rect {
    float x, y, w, h;
};

using point = vec2;

struct triangle {
    unsigned int v[3];

    unsigned int operator[](unsigned int index) const {
        return v[index];
    }

    unsigned int &operator[](unsigned int index) {
        return v[index];
    }
};

struct circle {
    vec2 middle;
    float rad;
};

class triangulation {
    public :
        std::vector<triangle> tlist;
        std::vector<point> plist;
        void triangulize(std::vector<point> &point_list);
};
