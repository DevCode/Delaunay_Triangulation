#include <cmath>

#include <limits>
#include <iostream>

#include "SDL2/SDL.h"
#include "SDL2/SDL2_gfxPrimitives.h"

#include "delaunay_triang.h"
#include "Linear_Algebra/vector.h"

static void get_circumcicle_triangle(triangle &t, std::vector<point> &plist, circle &dst) {
    // middlepoints of the axes 
    vec2 p1 = plist[t.v[0]] + ((plist[t.v[1]] - plist[t.v[0]]) * 0.5f);
    vec2 p3 = plist[t.v[0]] + ((plist[t.v[2]] - plist[t.v[0]]) * 0.5f);

    // get normals of the axes
    vec2 n_ab = (plist[t.v[1]] - plist[t.v[0]]).get_ortho_vec();
    vec2 n_ac = (plist[t.v[2]] - plist[t.v[0]]).get_ortho_vec();

    // second points for intersection formula
    vec2 p2 = p1 + n_ab;
    vec2 p4 = p3 + n_ac;

    // intersection between these two lines is midpoint of circumcircle
    dst.middle = intersection_lines(p1, p2, p3, p4);
    dst.rad = (dst.middle - plist[t.v[0]]).len();
}

static bool is_in_circumcicle_of_triangle(triangle &t, std::vector<point> &plist, point &p) {
    circle circumcircle;
    get_circumcicle_triangle(t, plist, circumcircle);

    return ((p - circumcircle.middle).len()) < circumcircle.rad;
}

static int index_of(triangle &t, point &p, std::vector<point> &plist) {
    for (int i = 0; i < 3; i++) {
        if (p == plist[t.v[i]])
            return i;
    }
    return -1;
}

// returns first index of point that t and t2 share
// return -1 if they aren't connected
static int share_side(triangle &t, triangle &t2, std::vector<point> &plist) {
    for (int i = 0; i < 3; i++) {
        int next = (i + 1) % 3;
        if (index_of(t2, plist[t.v[i]], plist) != -1 && index_of(t2, plist[t.v[next]], plist) != -1)
            return i;
    }
    return -1;
}

static void get_triangle_around_rect(point dst[3], rect &r) {
    dst[0][0] = r.x + (r.w / 2);
    dst[0][1] = r.y - (r.h);

    float m = (r.w / 2.0f) / (r.h / 2.0f);

    dst[1][0] = dst[0][0] - (int) (m * (r.h * 2.0f));
    dst[1][1] = r.y + r.h;
    dst[2][0] = dst[0][0] + (int) (m * (r.h * 2.0f));
    dst[2][1] = r.y + r.h;
}

static void get_triangle_around_pts(point dst[3], std::vector<point> &point_list) {
    point min_p = point_list[0];
    point max_p = point_list[0];
    rect bounds;

    for (size_t i = 0; i < point_list.size(); i++) {
        min_p[0] = point_list[i][0] > min_p[0] ? min_p[0] : point_list[i][0];
        min_p[1] = point_list[i][1] > min_p[1] ? min_p[1] : point_list[i][1];
        max_p[0] = point_list[i][0] < max_p[0] ? max_p[0] : point_list[i][0];
        max_p[1] = point_list[i][1] < max_p[1] ? max_p[1] : point_list[i][1];
    }

    bounds.x = min_p[0];
    bounds.y = min_p[1];
    bounds.w = (max_p[0] - min_p[0]);
    bounds.h = (max_p[1] - min_p[1]);

    get_triangle_around_rect(dst, bounds);
}

void triangulation::triangulize(std::vector<point> &point_list) {
    point start_p[3];
    triangle start_triangle;

    get_triangle_around_pts(start_p, point_list);

    plist.clear();
    tlist.clear();

    plist = point_list;

    plist.push_back(start_p[0]);
    plist.push_back(start_p[1]);
    plist.push_back(start_p[2]);

    start_triangle.v[0] = point_list.size();
    start_triangle.v[1] = point_list.size() + 1;
    start_triangle.v[2] = point_list.size() + 2;

    tlist.push_back(start_triangle);

    std::vector<triangle> bad_triangles;
    std::vector<int> polygon;

    for (std::vector<point>::size_type i = 0; i < point_list.size(); i++) {

        bad_triangles.clear();
        polygon.clear();
        // invalidate all triangles which circumcicle contains the new point
        // and add them to the bad_triangles array
        for (std::vector<triangle>::size_type j = 0; j < tlist.size(); j++) {
            if (is_in_circumcicle_of_triangle(tlist[j], plist, plist[i])) {
                bad_triangles.push_back(tlist[j]);
                tlist.erase(tlist.begin() + j--);
            }
        }

        // find all edges that are not shared by the other bad triangles
        // so you get a polygon which defines a cavity around the new point
        for (std::vector<triangle>::size_type j = 0; j < bad_triangles.size(); j++) {
            for (int l = 0; l < 3; l++) {
                bool found = false;

                for (std::vector<triangle>::size_type k = 0; k < bad_triangles.size() && (!found); k++) {
                    if (j == k) continue;

                    if (share_side(bad_triangles[j], bad_triangles[k], plist) == l)
                        found = true;
                }

                if (!found) {
                    polygon.push_back(bad_triangles[j].v[l]);
                    polygon.push_back(bad_triangles[j].v[(l + 1) % 3]);
                }
            }
        }

        // construct new triangles and insert them at the end (number_of_triangles)
        for (std::vector<int>::size_type j = 0; j < polygon.size(); j+=2) {
            triangle new_tri;
            new_tri.v[0] = polygon[j];
            new_tri.v[1] = polygon[j + 1];
            new_tri.v[2] = i;
            tlist.push_back(new_tri);
        }
    }

    // remove all triangles that are part of the "super triangle"
    for (std::vector<triangle>::size_type i = 0; i < tlist.size(); i++) {
        for (int j = 0; j < 3; j++) {
            if (index_of(tlist[i], plist[start_triangle.v[j]], plist) != -1) {
                tlist.erase(tlist.begin() + i--);
                break;
            }
        }
    }
}
